import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'mainreg.dart';
import 'navigationBar.dart';

void main() {
  runApp(const ColumnExample());
}

// class RegPage extends StatefulWidget {
//   @override
//   State<RegPage> createState() => _RegPage();
// }
//
// class _RegPage extends State<RegPage> {
//   @override
//   Widget build(BuildContext context) {
//     return DevicePreview(
//       tools: const [
//         DeviceSection(),
//       ],
//       builder: (context) => MaterialApp(
//         debugShowCheckedModeBanner: false,
//         useInheritedMediaQuery: true,
//         builder: DevicePreview.appBuilder,
//         locale: DevicePreview.locale(context),
//         title: 'ระบบบริการการศึกษา มหาวิทยาลัยบูรพา',
//         theme: ThemeData(
//           primarySwatch: Colors.grey,
//         ),
//         home: Scaffold(
//           backgroundColor: Colors.white,
//           appBar: AppBar(
//             title: Text(
//               "Burapha University",
//               style: TextStyle(
//                 fontWeight: FontWeight.w400,
//                 fontSize: 28.0,
//               ),
//             ),
//           ),
//           drawer: NavigationDrawer(),
//         ),
//       ),
//     );
//   }
// }
//
// class NavigationDrawer extends StatelessWidget {
//
//   @override
//   Widget build(BuildContext context) => Drawer(
//     child: SingleChildScrollView(
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         children: <Widget>[
//           buildHeader(context),
//           buildMenuItems(context),
//         ],
//       ),
//     ),
//   );
//
//   Widget buildHeader(BuildContext context) => Container(
//     padding: EdgeInsets.only(
//       top: MediaQuery.of(context).padding.top,
//     ),
//   );
//
//   Widget buildMenuItems(BuildContext context) => Column(
//     children: [
//       ListTile(
//         leading: const Icon(Icons.menu),
//         // title: const Text("Home"),
//         onTap: () {},
//       ),
//       ListTile(
//         leading: const Icon(Icons.menu_book),
//         title: const Text("ตารางเรียนนิสิต", style: TextStyle(fontSize: 24)),
//         onTap: () =>
//             Navigator.of(context).push(MaterialPageRoute(builder: (context) => const MyClass()))
//
//       ),
//       ListTile(
//         leading: const Icon(Icons.table_chart_outlined),
//         title: const Text("ตารางการใช้ห้อง", style: TextStyle(fontSize: 24)),
//         onTap: () {},
//       ),
//       ListTile(
//         leading: const Icon(Icons.calendar_month),
//         title: const Text("ปฏิทินการศึกษา", style: TextStyle(fontSize: 24)),
//         onTap: () {},
//       ),
//       ListTile(
//         leading: const Icon(Icons.question_answer_rounded),
//         title: const Text("ตอบคำถาม", style: TextStyle(fontSize: 24)),
//         onTap: () {},
//       ),
//       ListTile(
//         leading: const Icon(Icons.app_registration),
//         title: const Text("แนะนำการลงทะเบียน", style: TextStyle(fontSize: 24)),
//         onTap: () {},
//       ),
//     ],
//   );
// }
//
// class MyClass extends StatelessWidget {
//   const MyClass({Key? key}) : super(key:key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         backgroundColor: Colors.grey,
//         title: const Text(
//           "ตารางเรียนนิสิต",
//           style: TextStyle(fontWeight: FontWeight.w400, fontSize: 30),
//         ),
//       ),
//     );
//   }
// }

class ColumnExample extends StatelessWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'ระบบบริการการศึกษา มหาวิทยาลัยบูรพา',
        theme: ThemeData(
          // primarySwatch: Colors.grey,

        ),
        home: Scaffold(
          drawer: navBar(),
          backgroundColor: Colors.yellow.shade50,
          appBar: AppBar(
            // title: const Text('REG Burapha University'),
            title: const Text('Burapha University', style: TextStyle(
              fontWeight: FontWeight.w500,
                 fontSize: 30.0,

               ),),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.logout),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  );
                },

              )
            ],

            backgroundColor: Colors.yellow.shade700,
          ),
          body:SingleChildScrollView(
            child: RegPage(),
          ),
        ),
      ),
    );
  }
}

class LoginPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade800,
        title: const Text(
          "เข้าสู่ระบบ",
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 30),
        ),
      ),
    );
  }
}








