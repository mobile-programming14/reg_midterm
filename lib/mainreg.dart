import 'package:flutter/material.dart';

class RegPage extends StatelessWidget {
  const RegPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Center(
            child: Text(
              '1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565(ด่วน)',
              style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Image.network(
            "img/grad652.png",
            height: 260, width: 250,
          ),

          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              '2.LINE Official',
              style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Image.network(
              "img/Line.jpg",
              height: 240,),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              'ติดต่อ',
              style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.red),
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              'ฝ่ายรับเข้าศึกษาระดับปริญญาตรีโทร. \n 038-102721 หรือ 038-102643',
              style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,),
            ),
          ),

        ],
      ),
    );
  }
}


