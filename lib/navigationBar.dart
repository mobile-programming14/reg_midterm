import 'package:flutter/material.dart';

class navBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Remove padding
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('ธัญญ่า อัครวิมลกุล', style: TextStyle(fontSize: 26)),
            accountEmail: Text('63160284@go.buu.ac.th', style: TextStyle(fontSize: 22)),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZEu8Z9gtekyG1fHGuJidolsHv38kcTcS20HqZ0Ju-YKLfUWMyuz8GATQ_obgtrvIr2j8&usqp=CAU',
                  fit: BoxFit.cover,
                  width: 90,
                  height: 90,
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.yellow.shade700,
            ),
          ),

      ListTile(
        leading: const Icon(Icons.menu_book),
        title: const Text("ตารางเรียนนิสิต", style: TextStyle(fontSize: 24)),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const MyClass()))
      ),
      ListTile(
        leading: const Icon(Icons.table_chart_outlined),
        title: const Text("ตารางการใช้ห้อง", style: TextStyle(fontSize: 24)),
        onTap:  () =>
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const RoomTable()))
      ),
      ListTile(
        leading: const Icon(Icons.calendar_month),
        title: const Text("ปฏิทินการศึกษา", style: TextStyle(fontSize: 24)),
        onTap: () =>
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Calender()))
      ),
      ListTile(
        leading: const Icon(Icons.question_answer_rounded),
        title: const Text("ตอบคำถาม", style: TextStyle(fontSize: 24)),
        onTap: () =>
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const QA()))
      ),
      ListTile(
        leading: const Icon(Icons.app_registration),
        title: const Text("แนะนำการลงทะเบียน", style: TextStyle(fontSize: 24)),
        onTap: () =>
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Advice()))
      ),
        ],
      ),
    );
  }
}

class MyClass extends StatelessWidget {
  const MyClass({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade800,
        title: const Text(
          "ตารางเรียนนิสิต",
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 30),
        ),
      ),
    );
  }
}

class RoomTable extends StatelessWidget {
  const RoomTable({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade800,
        title: const Text(
          "ตารางการใช้ห้อง",
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 30),
        ),
      ),
    );
  }
}

class Calender extends StatelessWidget {
  const Calender({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade800,
        title: const Text(
          "ปฏิทินการศึกษา",
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 30),
        ),
      ),
    );
  }
}

class QA extends StatelessWidget {
  const QA({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade800,
        title: const Text(
          "ตอบคำถาม",
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 30),
        ),
      ),
    );
  }
}

class Advice extends StatelessWidget {
  const Advice({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade800,
        title: const Text(
          "แนะนำการลงทะเบียน",
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 30),
        ),
      ),
    );
  }
}